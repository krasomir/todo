@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h2 class="d-inline-block">ToDo statistics</h2>
                    <span class="d-inline-block float-right text-primary" data-toggle="modal" data-target="#newEditTodoModal">
                        <i class="far fa-plus-square"></i>
                    </span>
                </div>

                <div class="card-body">

                    <ul class="list-group">
                        <li class="list-group-item">
                            <p>Active ToDo's: {{ $numberOfActiveTodos }}</p>
                            <p>Unactive ToDo's: {{ $numberOfUnactiveTodos }}</p>
                            <p>Completed ToDo's: {{ $numberOfCompletedTodos }}</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
    <!-- new and edit modal -->
    <div class="modal fade" id="newEditTodoModal" tabindex="-1" role="dialog" aria-labelledby="newEditTodoModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title" id="newEditTodoModalLabel">Add new ToDo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="newEditForm" action="{{ route('save-todo') }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="tile">Title</label>
                            <input type="text" name="title" id="title" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="tile">Description</label>
                            <textarea name="description" id="description" class="form-control" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id" id="id" value="">
                        <input type="hidden" name="type" id="type" value="insert">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection