@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h2 class="d-inline-block">All ToDo's</h2>
                    <span class="d-inline-block float-right text-primary" data-toggle="modal" data-target="#newEditTodoModal">
                        <i class="far fa-plus-square"></i>
                    </span>
                </div>

                <div class="card-body">
                    @include('shared.alert')

                    <ul class="list-group">
                        @if(count($todos) > 0)
                            <input type="hidden" name="completeRoute" id="completeRoute" value="{{ route('complete-todo') }}">
                            @foreach($todos as $todo)
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col align-self-center">
                                        <input type="checkbox" name="todoItem" value="{{ $todo->id }}" class="form-control">
                                    </div>
                                    <div class="col col-sm-8 align-self-center">
                                        <span class="title font-weight-bold">
                                            <a href="#" data-toggle="modal" data-target="#infoTodoModal" data-description="{{ $todo->description }}" data-title="{{ $todo->title }}">{{ $todo->title }}</a>
                                        </span>
                                    </div>
                                    <div class="col align-self-center">
                                        <div data-toggle="modal" data-target="#newEditTodoModal" data-id="{{ $todo->id }}" data-description="{{ $todo->description }}" data-title="{{ $todo->title }}" class="text-right text-info">
                                            <i class="fas fa-pen-square"></i>
                                        </div>
                                        <div data-toggle="modal" data-target="#confirmDeleteTodoModal" data-id="{{ $todo->id }}" class="text-right text-danger">
                                            <i class="fas fa-trash-alt"></i>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        @else
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col align-self-center">
                                    There are no unfinished ToDo's... 
                                    <button type="button" class="btn btn-sm btn-outline-primary float-right" data-toggle="modal" data-target="#newEditTodoModal">Add one</button>
                                </div>
                            </div>
                        </li>
                        @endif
                    </ul>
                    <p>{{ $todos->links() }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
    <!-- new and edit modal -->
    <div class="modal fade" id="newEditTodoModal" tabindex="-1" role="dialog" aria-labelledby="newEditTodoModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title" id="newEditTodoModalLabel">Add new ToDo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="newEditForm" action="{{ route('save-todo') }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="tile">Title</label>
                            <input type="text" name="title" id="title" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="tile">Description</label>
                            <textarea name="description" id="description" class="form-control" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id" id="id" value="">
                        <input type="hidden" name="type" id="type" value="insert">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- info modal -->
    <div class="modal fade" id="infoTodoModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- confirm delete modal -->
    <div class="modal fade" id="confirmDeleteTodoModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h5 class="modal-title">Delete ToDo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Do you really want to delete ToDo?</p>
                </div>
                <div class="modal-footer">
                    <form method="POST">
                        @csrf
                        <input type="hidden" name="deleteId" id="deleteId" value="">
                        <input type="hidden" name="deleteRoute" id="deleteRoute" value="{{ route('delete-todo') }}">
                    </form>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="deleteTodoBtn" type="button" class="btn btn-danger">Delete</button>
                </div>
            </div>
        </div>
    </div>
@endsection
