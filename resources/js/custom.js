$(document).ready(function(){
    
    /**
     * display info data
     */
    $("#infoTodoModal").on('show.bs.modal', function(e) {
        
        let button = $(e.relatedTarget);
        let title = button.data('title');
        let description = button.data('description');
        
        $(this).find('.modal-title').text(title);
        $(this).find('.modal-body').html('<p>'+ description +'</p>');

    });

    /**
     * fill inputs with data for edit
     */
    $("#newEditTodoModal").on('show.bs.modal', function (e) {
        
        $("#newEditForm input").not('input[name="_token"]').val('');
        $("#newEditForm textarea").empty();

        let button = $(e.relatedTarget);
        let id = button.data('id');
        let title = button.data('title');
        let description = button.data('description');

        if (id) {
            $(this).find('.modal-title').text('Edit ToDo');
            $(this).find('#id').val(id);
            $(this).find('#type').val('edit');
            $(this).find('#title').val(title);
            $(this).find('#description').text(description);
        } else {
            $(this).find('.modal-title').text('Add new ToDo');
            $(this).find('#type').val('insert');
        }
    });

    /**
     * open confirm delete modal
     */
    $("#confirmDeleteTodoModal").on('show.bs.modal', function (e) {
        
        $("#deleteMsg").remove();
        let button = $(e.relatedTarget);
        let deleteId = button.data('id');

        $(this).find('#deleteId').val(deleteId);
    });

    /**
     * deactivate todo
     */
    $("#deleteTodoBtn").on('click', function (e) {
        
        let id = $("#deleteId").val();
        let token = $("input[name=_token]").val();
        let url = $("#deleteRoute").val();
        let data = {
            id, 
            '_token':token,   
            '_method':'PATCH',         
        }

        axios.post(url, data)
            .then(response => {
                let msg = '';
                if (response.data.error === true) {
                    $.each(response.data.messages, function (key, value) {
                        msg += '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button>'+ value +'</div>';
                    });
                } else {
                    msg = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>ToDo successfully deleted</div>';
                }
                $('<div id="deleteMsg">'+ msg +'</div>').insertAfter("#app .container .card-body > .row");
                $("#confirmDeleteTodoModal").modal('hide');
                location.reload();
            })
            .catch (response => {
                console.log(response);
            });
    });

    /**
     * mark todo as completed
     */
    $("#app .list-group input[type=checkbox]").on('change', function (e) {
        
        let id = $(this).val();
        let url = $("#completeRoute").val();
        let token = $("input[name=_token]").val();
        let data = {
            id, 
            '_token':token,   
            '_method':'PATCH',
        }

        axios.post(url, data)
            .then(response => {
                let msg = '';
                if (response.data.error === true) {
                    $.each(response.data.messages, function (key, value) {
                        msg += '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button>'+ value +'</div>';
                    });
                } else {
                    msg = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>ToDo marked as completed</div>';
                }
                $('<div id="deleteMsg">'+ msg +'</div>').insertAfter("#app .container .card-body > .row");
                location.reload();
            })
            .catch(response => {
                console.log(response);
            });
    });
    
});