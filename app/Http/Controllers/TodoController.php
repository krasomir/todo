<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TodoController extends Controller
{
    private $Todo;
    
    public function __construct()
    {
        $this->Todo = new Todo();
    }

    public function index()
    {
        $todos = $this->Todo->getUserActiveCompletedTodos(1,0);

        return view('home')->with('todos', $todos);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function saveTodo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'type' => 'required|in:insert,edit',
            'id' => 'nullable|required_if:type,edit|numeric'
        ]);

        if ($validator->fails()) {
            return redirect()->route('home')->withErrors($validator);
        }

        if ($this->Todo->saveTodo($request)) {
            return redirect()->route('home')->with('success', 'ToDo successfully ' . $request->type . 'ed');
        }

        return redirect()->route('home')->withErrors($validator);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function deleteTodo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'messages' => $validator->errors()
            ], 200);
        }

        if ($this->Todo->deactivateTodo($request)) {
            return response()->json([
                'error' => false,
                'messages' => 'ToDo seccessfully deleted!'
            ], 200);
        }

        return response()->json([
            'error' => true,
            'messages' => 'Error deleting ToDo!'
        ], 500);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function completeTodo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'messages' => $validator->errors()
            ], 200);
        }

        if ($this->Todo->completeTodo($request)) {
            return response()->json([
                'error' => false,
                'messages' => 'ToDo marked as completed!'
            ], 200);
        }

        return response()->json([
            'error' => true,
            'messages' => 'Error completing ToDo!'
        ], 500);
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getStatistics()
    {
        return view('statistics')->with([
            'numberOfActiveTodos' => count($this->Todo->getUserActiveCompletedTodos(1, 0, true)),
            'numberOfUnactiveTodos' => count($this->Todo->getUserActiveCompletedTodos(0, 0, true)),
            'numberOfCompletedTodos' => count($this->Todo->getUserActiveCompletedTodos(1, 1, true))
        ]);
    }
}
