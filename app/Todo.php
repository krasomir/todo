<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Todo extends Model
{
    protected $fillable = [
        'user_id', 'title', 'description', 'status'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Undocumented function
     *
     * @param [type] $query
     * @return void
     */
    public function scopeActive($query, $activity)
    {
        return $query->where('status', $activity);
    }

    /**
     * Undocumented function
     *
     * @param [type] $query
     * @return void
     */
    public function scopeCompleted($query, $completnes)
    {
        return $query->where('completed', $completnes);
    }

    /**
     * Undocumented function
     *
     * @param integer $isActive
     * @param integer $isComplete
     * @param boolean $count
     * @return void
     */
    public function getUserActiveCompletedTodos(int $isActive, int $isComplete, $count = false)
    {
        if ($count) {
            return $this->where('user_id', Auth::user()->id)
                ->active($isActive)
                ->completed($isComplete)
                ->get();
        }
        return $this->where('user_id', Auth::user()->id)
            ->active($isActive)
            ->completed($isComplete)
            ->paginate(5);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function saveTodo(Request $request)
    {
        $todo = ($request->type === 'insert') ? $this: $this->find($request->id);

        $todo->title = $request->title;
        $todo->description = $request->description;
        $todo->user_id = Auth::user()->id;
        
        return $todo->save();
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function deactivateTodo(Request $request)
    {
        $todo = $this->find($request->id);
        $todo->status = 0;

        return $todo->save();
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function completeTodo(Request $request)
    {
        $todo = $this->find($request->id);
        $todo->completed = 1;

        return $todo->save();
    }
}
