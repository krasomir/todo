<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::middleware('auth')->group(function () {

    Route::get('/home', 'TodoController@index')->name('home');
    Route::get('/statistics','TodoController@getStatistics')->name('get-statistics');
    
    Route::post('/save-todo','TodoController@saveTodo')->name('save-todo');
    Route::patch('/delete-todo', 'TodoController@deleteTodo')->name('delete-todo');
    Route::patch('/complete-todo', 'TodoController@completeTodo')->name('complete-todo');
});


